# frozen_string_literal: true

module ReleaseTools
  module Deployments
    class BlockerIssue
      def initialize(data)
        @data = data
      end

      def title
        "#{data.web_url}+"
      end

      def root_cause
        return unless root_cause_label

        "~\"#{root_cause_label}\""
      end

      def hours_gstg_blocked
        hrs_blocked(environment: 'gstg')
      end

      def hours_gprd_blocked
        hrs_blocked(environment: 'gprd')
      end

      attr_reader :data

      private

      def hrs_blocked(environment:)
        labels
          .grep(/Deploys-blocked-#{environment}/)
          .first&.gsub(/[^0-9.]/, '')
          .to_f
      end

      def root_cause_label
        labels
          .detect { |label| label.include?('RootCause') }
      end

      def labels
        data.labels
      end
    end
  end
end
