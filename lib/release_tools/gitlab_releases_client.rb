# frozen_string_literal: true

require 'gitlab_releases'

module ReleaseTools
  # Utility methods for interacting with the releases gem https://gitlab.com/gitlab-org/delivery/gitlab-releases
  class GitlabReleasesClient
    class << self
      extend Forwardable

      def_delegator :client, :active_version
      def_delegator :client, :version_for_date
      def_delegator :client, :upcoming_releases
      def_delegator :client, :previous_minors
      def_delegator :client, :current_minor_for_date
    end

    def self.client
      @client ||= GitlabReleases
    end
  end
end
