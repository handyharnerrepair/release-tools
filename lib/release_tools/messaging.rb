# frozen_string_literal: true

module ReleaseTools
  class Messaging
    COMMENT_FOOTNOTE = <<~FOOTNOTE.strip
      <hr>

      <sub>
        :robot: This is an automated message generated using the
        [release tools project](https://gitlab.com/gitlab-org/release-tools/).
        If you believe there is an error, please create an issue in the
        release tools project.
      </sub>
    FOOTNOTE
  end
end
