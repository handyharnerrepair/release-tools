# frozen_string_literal: true

module ReleaseTools
  module Monthly
    module Finalize
      class UpdateProtectedBranches
        include ::SemanticLogger::Loggable

        GITLAB_RELEASE_TOOLS_BOT_ID = 2_324_599
        GITLAB_BOT_ID = 1_786_152
        RELEASE_MANAGERS_GROUP_ID = 2_584_649
        MAINTAINER_ACCESS_LEVEL = 40 # maintainer

        PROJECTS = [ReleaseTools::Project::GitlabEe, ReleaseTools::Project::OmnibusGitlab].freeze

        def execute
          PROJECTS.each do |project|
            logger.info('Unprotecting previous stable branch', branch: old_branch(ee: project.ee_branch?), project: project)
            logger.info('Protecting new stable branch', branch: new_branch(ee: project.ee_branch?), project: project)

            next if SharedStatus.dry_run?

            Retriable.with_context(:api) do
              client.unprotect_branch(project, old_branch(ee: project.ee_branch?))
            end

            Retriable.with_context(:api) do
              client.protect_branch(
                project,
                new_branch(ee: project.ee_branch?),
                merge_access_level: MAINTAINER_ACCESS_LEVEL,
                allowed_to_merge: allowed_to_merge,
                allowed_to_push: allowed_to_push,
                code_owner_approval_required: false
              )
            end
          end

          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(error_message, error: ex)

          send_slack_notification(:failed)

          raise
        end

        private

        def new_branch(ee:)
          ReleaseTools::Version.new(current_version.next_minor).stable_branch(ee: ee)
        end

        def old_branch(ee:)
          current_version.stable_branch(ee: ee)
        end

        def current_version
          ReleaseTools::Versions.current_version
        end

        def allowed_to_merge
          [
            { user_id: GITLAB_RELEASE_TOOLS_BOT_ID },
            { user_id: GITLAB_BOT_ID }
          ]
        end

        def allowed_to_push
          [
            { group_id: RELEASE_MANAGERS_GROUP_ID },
            { user_id: GITLAB_RELEASE_TOOLS_BOT_ID },
            { user_id: GITLAB_BOT_ID }
          ]
        end

        def client
          ReleaseTools::GitlabClient
        end

        def error_message
          <<~MSG
            Updating the stable branch protection rules failed. If this continues to fail, manually complete the following for
            the `gitlab-org/gitlab` and `gitlab-org/omnibus-gitlab` projects.

            1. Protect the current stable branch with the following rules:
              - code_owner_approval_required: false
              - allowed to merge: maintainers
              - allowed to push and merge: @gitlab-bot, @gitlab-release-tools-bot, @gitlab-org/release/managers group
            2. Unprotect the previous stable branch.
          MSG
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Update Protected Branches',
            status: status,
            release_type: :monthly
          ).send_notification
        end
      end
    end
  end
end
