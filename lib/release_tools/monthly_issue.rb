# frozen_string_literal: true

module ReleaseTools
  class MonthlyIssue < Issue
    def create
      monthly_release_pipeline if monthly_release_pipeline?

      super
    end

    def title
      "Release #{version.to_minor}"
    end

    def labels
      'Monthly Release,team::Delivery'
    end

    def project
      ::ReleaseTools::Project::Release::Tasks
    end

    def assignees
      ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
    rescue ReleaseManagers::Schedule::VersionNotFoundError
      nil
    end

    def current_stable_branch
      ReleaseTools::Versions.current_stable_branch
    end

    def monthly_release_pipeline?
      Feature.enabled?(:monthly_release_pipeline)
    end

    def monthly_release_pipeline
      @monthly_release_pipeline ||= GitlabOpsClient.create_pipeline(
        Project::ReleaseTools,
        MONTHLY_RELEASE_PIPELINE: 'true'
      )
    end

    def release_date
      @release_date ||= Date.parse(
        ReleaseTools::GitlabReleasesClient.upcoming_releases[version.to_minor]
      )
    end

    def formated_date(date)
      date.strftime("%A, %b %-d")
    end

    def preparation_start_day
      formated_date(release_date - 6.days)
    end

    def candidate_selection_day
      formated_date(release_date - 3.days)
    end

    def rc_tag_day
      formated_date(release_date - 2.days)
    end

    def tag_day
      formated_date(release_date - 1.day)
    end

    def release_day
      formated_date(release_date)
    end

    def ordinalized_release_date
      release_date.day.ordinalize
    end

    protected

    def template_path
      File.expand_path('../../templates/monthly.md.erb', __dir__)
    end
  end
end
