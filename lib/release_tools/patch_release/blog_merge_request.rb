# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class BlogMergeRequest < MergeRequest
      include ::SemanticLogger::Loggable
      include ReleaseTools::Security::IssueHelper

      # Projects that are not included in the table of fixes because their
      # fixes are often due to external CVEs
      NON_TABLE_OF_FIXES_PROJECTS = [
        Project::OmnibusGitlab.security_id,
        Project::CNGImage.security_id
      ].freeze

      CRITICAL_RELEASE_LABEL = "severity::1"

      def labels
        'patch release post'
      end

      def title
        "Draft: Adding #{versions_str} blog post"
      end

      def assignee_ids
        if security_content?
          appsec_release_managers.collect(&:id)
        else
          release_managers.collect(&:id)
        end
      end

      def source_branch
        "create-#{hyphenated_version}-post"
      end

      def create
        return unless create_blog_post_file

        super
      end

      def patch_issue_url
        if dry_run?
          'test.gitlab.com'
        else
          patch_issue.url
        end
      end

      def generate_blog_content
        raise 'content is not provided' unless patch_content || security_content

        ERB.new(content_template_path, trim_mode: '-').result(binding)
      end

      def blog_post_filename
        if includes_security_content?
          "#{Date.current + 1.day}-security-release-gitlab-#{hyphenated_version}-released.html.md"
        else
          "#{Date.current}-gitlab-#{hyphenated_version}-released.html.md"
        end
      end

      def metadata_canonical_path
        # this value is not used outside of security release blog posts
        return unless includes_security_content?

        date = Date.current + 1.day

        "/releases/#{date.strftime('%Y/%m/%d')}/security-release-gitlab-#{hyphenated_version}-released.html.md"
      end

      def security_issue_header(security_issue)
        security_issue&.cves_issue&.title || security_issue.issue.title
      end

      def security_issue_blog_content(security_issue)
        cves_issue = security_issue.cves_issue

        if cves_issue
          <<~STR
            #{cves_issue.vulnerability_description.gsub(/[.]+\z/, '')}.
            This is a #{cves_issue.cvss_severity.downcase} severity issue (`#{cves_issue.cvss_string}`, #{cves_issue.cvss_base_score}).
            #{cves_id_link(cves_issue)}

            #{cves_issue.credit.gsub(/[.]+\z/, '')}.
          STR
        else
          <<~STR
            TODO: add description for this issue
          STR
        end
      end

      def cves_id_link(cves_issue)
        if cves_issue.cve_id.blank?
          "We have requested a CVE ID and will update this blog post when it is assigned."
        else
          "It is now mitigated in the latest release and is assigned [#{cves_issue.cve_id}](https://cve.mitre.org/cgi-bin/cvename.cgi?name=#{cves_issue.cve_id})."
        end
      end

      def project_path
        # If the blog post contains security content, the blog MR needs
        # to be created in the security mirror.
        return project.security_path if includes_security_content?

        super
      end

      def sorted_security_content
        security_content
          .sort_by { |issue| issue.cves_issue&.cvss_base_score || -1 }
          .reverse
      end

      def sorted_table_content
        sorted_security_content.reject do |issue|
          NON_TABLE_OF_FIXES_PROJECTS.include?(issue.project_id)
        end
      end

      def security_issue_slug(security_issue)
        security_issue_header(security_issue)
          .downcase
          .scan(/\w|\d| |-/)
          .join
          .tr(' ', '-')
      end

      def displayed_severity(security_issue)
        severity = security_issue.cves_issue&.cvss_severity
        return 'TODO' if severity == 'None'

        severity
      end

      def blog_metadata_title
        "GitLab #{title_header} Security Release: #{versions_str}".squish
      end

      def blog_metadata_description
        "Learn more about GitLab #{title_header} Security Release: #{versions_str} for GitLab Community Edition (CE) and Enterprise Edition (EE).".squish
      end

      protected

      def hyphenated_version
        versions.first.tr(', ', '--').tr('.', '-')
      end

      def template_path
        File.expand_path('../../../templates/patch_blog_post_merge_request_description.md.erb', __dir__)
      end

      def content_template_path
        path = if includes_security_content?
                 'security_release_blog_post_template.html.md.erb'
               else
                 'patch_release_blog_post_template.html.md.erb'
               end

        File.read("templates/blog_posts/#{path}")
      end

      def create_blog_post_file
        file_content = generate_blog_content

        filepath = blog_post_filepath + blog_post_filename

        logger.info('Created blog post file')

        unless SharedStatus.dry_run?
          Retriable.with_context(:api) do
            GitlabClient.find_or_create_branch(source_branch, target_branch, project_path)
          end
        end

        logger.info('Created branch', branch_name: source_branch, project: project_path)

        commit(file_content, filepath)
      end

      def commit(file_content, filepath, action = 'create', message = nil)
        message ||= "Adding #{versions_str} blog post"

        actions = [{
          action: action,
          file_path: filepath,
          content: file_content
        }]

        logger.info('Committing blog content', project: project_path, branch: source_branch)

        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          GitlabClient.create_commit(
            project_path,
            source_branch,
            message,
            actions
          )
        end
      end

      def blog_post_filepath
        "sites/uncategorized/source/releases/posts/"
      end

      def patch_issue
        @patch_issue ||= Issue.new
      end

      def release_managers
        schedule.active_release_managers
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        logger.fatal('Could not find active release managers')
        nil
      end

      def appsec_release_managers
        schedule.active_appsec_release_managers
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        logger.fatal('Could not find active appsec release managers')
        nil
      end

      def schedule
        @schedule ||= ReleaseManagers::Schedule.new
      end

      def versions
        if includes_security_content?
          patch_content.map { |release| release[:version] }
        else
          patch_content.filter_map do |release|
            release[:version] if release[:pressure].positive?
          end
        end
      end

      def versions_str
        versions.join(', ')
      end

      def security_content?
        !security_content.blank?
      end

      def critical_release?
        return nil unless security_content?

        security_content.any? { |issue| issue.labels.include?(CRITICAL_RELEASE_LABEL) }
      end

      def includes_security_content?
        security_content?
      end

      def title_header
        critical_release? ? 'Critical' : ''
      end
    end
  end
end
