# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class SecurityIssue < Issue
      include ::ReleaseTools::Security::ComponentBranchHelper
      include ::ReleaseTools::Security::IssueHelper

      def create
        security_pipeline

        super
      end

      def title
        if regular?
          "Security patch release: #{versions_title}"
        else
          "Critical security patch release: #{versions_title}"
        end
      end

      def confidential?
        true
      end

      def labels
        "#{super},security"
      end

      def critical?
        ReleaseTools::SharedStatus.critical_security_release?
      end

      def regular?
        !critical?
      end

      def version
        ReleaseTools::Versions.latest(versions, 1).first
      end

      def versions
        patch_release_coordinator.versions
      end

      def blog_post_merge_request
        # This overrides the method defined in PatchRelease::Issue
        # to do nothing since we do not have a blog merge request class for security issues.
      end

      def add_blog_mr_to_description
        # This overrides the method defined in PatchRelease::Issue
        # to do nothing since we have not automated the blog merge request for security issues.
      end

      # Returns lists of projects that are not automatically processed by release tooling
      def unsupported_projects_list
        ReleaseTools::ManagedVersioning::PROJECTS_NEEDING_MANUAL_RELEASES
          .map(&:metadata_project_name)
          .join(', ')
      end

      def security_release_tracking_issue
        security_tracking_issue
      end

      def security_pipeline
        @security_pipeline ||= GitlabOpsClient.create_pipeline(
          Project::ReleaseTools,
          SECURITY_RELEASE_PIPELINE: 'true',
          SECURITY: ENV.fetch('SECURITY', nil)
        )
      end

      def version_type
        if critical?
          'Critical'
        else
          'Non Critical'
        end
      end

      protected

      def template_path
        File.expand_path('../../../templates/security_patch.md.erb', __dir__)
      end

      def patch_release_coordinator
        ReleaseTools::PatchRelease::Coordinator.new
      end
    end
  end
end
