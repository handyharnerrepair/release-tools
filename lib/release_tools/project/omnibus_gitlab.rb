# frozen_string_literal: true

module ReleaseTools
  module Project
    class OmnibusGitlab < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-org/omnibus-gitlab.git',
        dev:       'git@dev.gitlab.org:gitlab/omnibus-gitlab.git',
        security:  'git@gitlab.com:gitlab-org/security/omnibus-gitlab.git'
      }.freeze

      IDS = {
        canonical: 20_699,
        security: 15_667_093
      }.freeze

      def self.metadata_project_name
        'omnibus-gitlab-ee'
      end

      def self.ee_tag?
        true
      end

      def self.tag_for(version)
        version.to_omnibus(ee: ee_tag?)
      end
    end
  end
end
