# frozen_string_literal: true

module ReleaseTools
  module Promotion
    module Checks
      # ActiveIncidents checks for active incident issues with labels that block
      # deployments
      class ActiveIncidents
        include ProductionIssueTracker

        # @param scope [Symbol] can be either :deployment or :feature_flag.
        def initialize(scope:)
          @scope = scope
        end

        def name
          labels = blocking_labels.map { |l| %(~"#{l}") }.join(' / ')
          "active #{labels} incidents"
        end

        def labels
          'Incident::Active'
        end

        private

        attr_reader :scope
      end
    end
  end
end
