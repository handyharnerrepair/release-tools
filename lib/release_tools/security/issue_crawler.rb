# frozen_string_literal: true

module ReleaseTools
  module Security
    # Crawling of security release issues to determine their associated security
    # issues and merge requests.
    class IssueCrawler
      include ParallelMethods
      include ReleaseTools::Security::IssueHelper

      # The public project that is used for creating the security root issues.
      PUBLIC_PROJECT = 'gitlab-org/gitlab'

      # The namespace that security issues must reside in.
      SECURITY_NAMESPACE = 'gitlab-org/security'

      # The label required for a security implementation issue to be evaluated
      # for the next security release.
      SECURITY_TARGET_LABEL = 'security-target'

      SECURITY_NOTIFICATIONS_LABEL = 'security-notifications'

      # The date format used by issue due dates.
      DUE_DATE_FORMAT = '%Y-%m-%d'

      # The status of issues and merge requests to be considered
      OPENED = 'opened'

      # The status of issues and merge requests that are not considered.
      CLOSED = 'closed'

      # Returns all security issues for the upcoming security release.
      def upcoming_security_issues_and_merge_requests
        if security_tracking_issue
          related_security_issues
        else
          []
        end
      end

      # Returns all security issues for the given root security issue.
      #
      # The `release_issue_iid` is the IID of the (confidential) security issue
      # opened in the gitlab-org/gitlab project.
      def security_issues_for(release_issue_iid)
        related_issues = []

        GitlabClient
          .client
          .issue_links(PUBLIC_PROJECT, release_issue_iid)
          .auto_paginate do |issue|
            # The list of related issues may contain issues unrelated to
            # security merge requests. Since the issue links API does not have
            # any filtering capabilities, we must filter our unrelated issues
            # manually.
            next unless issue.state == OPENED
            next unless issue.web_url.include?(SECURITY_NAMESPACE)

            related_issues << issue
          end

        related_issues
      end

      # Returns issues that are related to the security release tracking issue.
      def related_security_issues
        security_issues_and_merge_requests_for(
          security_issues_for(security_tracking_issue.iid)
        )
      end

      # Returns issues on the security mirrors for projects under managed versioning that
      # are ready to be evaluated for the next security release.
      def evaluable_security_issues
        found_issues = ReleaseTools::ManagedVersioning::PROJECTS.flat_map do |project|
          GitlabClient.issues(project.security_path, labels: [SECURITY_TARGET_LABEL], state: OPENED)
        end.compact

        security_issues_and_merge_requests_for(found_issues)
      end

      def notifiable_security_issues_for(project)
        found_issues = GitlabClient.issues(
          project.security_path,
          labels: [SECURITY_NOTIFICATIONS_LABEL],
          state: OPENED
        )

        security_issues_and_merge_requests_for(found_issues)
      end

      # Returns all security issues and merge requests for a given set of
      # security issues.
      #
      # `related_issues` is an array of issues returned from GitlabClient
      def security_issues_and_merge_requests_for(related_issues)
        parallel_map(related_issues) do |issue|
          mrs = []

          GitlabClient
            .related_merge_requests(issue.project_id, issue.iid)
            .auto_paginate do |mr|
              next unless mr.web_url.include?(SECURITY_NAMESPACE)
              next if mr.state == CLOSED

              mrs << mr
            end

          ReleaseTools::Security::ImplementationIssue.new(issue, mrs)
        end
      end
    end
  end
end
