# frozen_string_literal: true

module ReleaseTools
  module Security
    # Validation of security issues linked to next security release tracking issue
    class IssueLinksValidator
      include ::SemanticLogger::Loggable
      include ParallelMethods
      include ::ReleaseTools::Security::IssueHelper

      COMMENT_TEMPLATE = <<~TEMPLATE.strip
        The following security issues have been unlinked from this tracking issue
        since they were added after the cutoff time of %<cutoff_time>s.

        If it is absolutely essential that these security issues be included in this
        security release, they can be linked as blockers to this security release.

        See the [security release process docs](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/process.md#security-release-deadlines)
        for more information about security release due dates.

        %<issues_strings>s

        #{Messaging::COMMENT_FOOTNOTE}
      TEMPLATE

      TIME_FORMAT = '%b %-d, %Y, %H:%M %Z'

      def initialize(client)
        @client = client
      end

      def execute
        return unless Feature.enabled?(:unlink_late_security_issues)

        logger.debug('Removing security issues added after cutoff', cutoff: issue_linking_cutoff_time, current_time: Time.current.utc)

        removed_issues = remove_issues_added_after_cutoff
        return if removed_issues.blank?

        add_comment_about_unlinked_issues(removed_issues)
      end

      private

      def remove_issues_added_after_cutoff
        # Terminate early if we've not yet reached the cutoff time.
        return if Time.current < issue_linking_cutoff_time

        linked_security_issues = IssueCrawler.new.security_issues_for(security_tracking_issue.iid)

        issues_to_remove =
          linked_security_issues.select do |issue|
            # Do not unlink security issues that block the security release.
            next if issue.link_type == 'is_blocked_by'

            Time.iso8601(issue.link_created_at) > issue_linking_cutoff_time
          end

        remove_issues(issues_to_remove)
      end

      def issue_linking_cutoff_time
        @issue_linking_cutoff_time ||= Date.parse(security_tracking_issue.due_date).in_time_zone('UTC') - 1.day
      end

      def remove_issues(issues)
        parallel_map(issues) do |issue|
          logger.info('Removing security issue added after cutoff', issue_url: issue.web_url, issue_link_created_at: issue.link_created_at, cutoff: issue_linking_cutoff_time)

          delete_issue_link(issue) unless ReleaseTools::SharedStatus.dry_run?

          issue
        end
      end

      def add_comment_about_unlinked_issues(issues)
        issues_strings = issues.collect { |issue| comment_issue_string(issue) }

        comment_body = format(
          COMMENT_TEMPLATE,
          cutoff_time: issue_linking_cutoff_time.strftime(TIME_FORMAT),
          issues_strings: issues_strings.join("\n")
        )

        logger.debug('Adding comment about removed issues', body: comment_body)

        return if ReleaseTools::SharedStatus.dry_run?

        @client.create_issue_note(
          security_tracking_issue.project_id,
          security_tracking_issue.iid,
          comment_body
        )
      end

      def comment_issue_string(issue)
        issue_usernames_string =
          if issue.assignees.present?
            issue.assignees.collect { |user| "@#{user.username}" }.join(', ')
          else
            "@#{issue.author.username}"
          end

        created_at = Time.iso8601(issue.link_created_at).utc.strftime(TIME_FORMAT)

        "1. #{issue.web_url} added on #{created_at} - #{issue_usernames_string}"
      end

      def delete_issue_link(issue)
        Retriable.with_context(:api) do
          GitlabClient
            .client
            .delete_issue_link(issue.project_id, issue.iid, issue.issue_link_id)
        end
      end
    end
  end
end
