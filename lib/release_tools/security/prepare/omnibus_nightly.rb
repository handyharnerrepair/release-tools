# frozen_string_literal: true

module ReleaseTools
  module Security
    module Prepare
      class OmnibusNightly
        include ::SemanticLogger::Loggable

        CouldNotUpdateError = Class.new(StandardError)

        PIPELINE_SCHEDULES = [
          'EE nightly',
          'CE nightly'
        ].freeze

        # @param action [Sym] - Accepts :enable or :disable
        def initialize(action:)
          @project = ReleaseTools::Project::OmnibusGitlab
          @client = ReleaseTools::GitlabDevClient
          @action = action
        end

        def execute
          pipeline_schedules.each do |pipeline_schedule|
            logger.info('Updating omnibus pipeline schedule', description: pipeline_schedule.description, action: action)

            next if SharedStatus.dry_run?

            take_ownership_of_pipeline(pipeline_schedule.id)
            update_pipeline_schedule(pipeline_schedule.id)
          rescue StandardError => ex
            logger.fatal(failure_message, error: ex)
            send_slack_notification(:failed)

            raise CouldNotUpdateError
          end

          send_slack_notification(:success)
        end

        private

        attr_reader :project, :client, :action

        def pipeline_schedules
          Retriable.with_context(:api) do
            client
              .pipeline_schedules(project)
              .select { |pipeline_schedule| PIPELINE_SCHEDULES.include?(pipeline_schedule.description) }
          end
        end

        def take_ownership_of_pipeline(pipeline_schedule_id)
          logger.info('Taking ownership of the pipeline schedule', pipeline_schedule: pipeline_schedule_id)

          Retriable.with_context(:api) do
            client.pipeline_schedule_take_ownership(
              project,
              pipeline_schedule_id
            )
          end
        end

        def update_pipeline_schedule(pipeline_schedule_id)
          Retriable.with_context(:api) do
            client.edit_pipeline_schedule(
              project.dev_path,
              pipeline_schedule_id,
              active: enabling_omnibus_builds?
            )
          end
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: job_type,
            status: status,
            release_type: :security
          ).send_notification
        end

        def job_type
          if enabling_omnibus_builds?
            'Enable Omnibus nightly builds'
          else
            'Disable Omnibus nightly builds'
          end
        end

        def enabling_omnibus_builds?
          action == :enable
        end

        def failure_message
          <<~MSG
            Updating Omnibus nightly builds failed. Ensure gitlab-release-tools-bot is the owner
            of the pipeline schedules and retry this job. If this job continues to fail,
            the schedules can be manually updated at https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipeline_schedules.
          MSG
        end
      end
    end
  end
end
