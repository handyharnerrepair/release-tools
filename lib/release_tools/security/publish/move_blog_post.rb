# frozen_string_literal: true

module ReleaseTools
  module Security
    module Publish
      class MoveBlogPost
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::MergeRequestHelper

        SecurityMergeRequestNotFoundError = Class.new(StandardError)

        def execute
          validate_existence_of_security_blog_post!

          logger.info(
            'Moving security blog post to canonical',
            merge_request: security_blog_merge_request.iid,
            url: security_blog_merge_request.web_url
          )

          return if SharedStatus.dry_run?

          copy_merge_request_to_canonical
          close_security_blog_merge_request
          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)
          send_slack_notification(:failed)

          raise
        end

        private

        def validate_existence_of_security_blog_post!
          return if security_blog_merge_request.present?

          logger.fatal('Security blog merge request not found')

          raise SecurityMergeRequestNotFoundError
        end

        def client
          ReleaseTools::GitlabClient
        end

        def copy_merge_request_to_canonical
          CopyBlogPostToCanonical.new(security_blog_merge_request).execute
        end

        def handbook_repository
          ReleaseTools::Project::WWWGitlabCom
        end

        def close_security_blog_merge_request
          logger.info(
            'Closing security blog merge request',
            url: security_blog_merge_request.web_url
          )

          Retriable.with_context(:api) do
            client.update_merge_request(
              handbook_repository.security_path,
              security_blog_merge_request.iid,
              {
                state_event: 'close'
              }
            )
          end

          Retriable.with_context(:api) do
            client.create_merge_request_comment(
              handbook_repository.security_path,
              security_blog_merge_request.iid,
              "This merge request has been moved to canonical at #{canonical_blog_merge_request.web_url}."
            )
          end
        end

        def failure_message
          <<~MSG
            The security blog merge request could not be found on
            https://gitlab.com/gitlab-org/security/www-gitlab-com/-/merge_requests.
            Check to see if the MR is there, add the `patch release post` label
            to it, and retry this job. It may be worth checking with AppSec to see if they are aware of
            any problems with the blog post.
          MSG
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Move blog post',
            status: status,
            release_type: :security
          ).send_notification
        end
      end
    end
  end
end
