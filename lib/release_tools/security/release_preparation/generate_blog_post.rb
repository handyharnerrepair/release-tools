# frozen_string_literal: true

module ReleaseTools
  module Security
    module ReleasePreparation
      class GenerateBlogPost
        include ReleaseTools::Security::IssueHelper
        include ::SemanticLogger::Loggable

        def execute
          blog_post = ReleaseTools::PatchRelease::BlogMergeRequest.new(
            project: ReleaseTools::Project::WWWGitlabCom,
            patch_content: coordinator.merge_requests(with_patch_version: true),
            security_content: ReleaseTools::Security::IssueCrawler.new.related_security_issues
          )

          if SharedStatus.dry_run?
            ReleaseTools.logger.info("Printing blog post for security release")

            puts blog_post.generate_blog_content
            return
          end

          blog_post.create

          notify_release_managers(blog_post)
          send_success_slack_notification(blog_post.url)
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)

          send_failed_slack_notification

          raise
        end

        private

        def notify_release_managers(blog_post)
          Retriable.with_context(:api) do
            ReleaseTools::GitlabClient.create_merge_request_comment(
              ReleaseTools::Project::WWWGitlabCom.security_path,
              blog_post.iid,
              "@gitlab-org/release/managers this is the security blog post for #{security_tracking_issue.web_url}."
            )
          end
        end

        def send_failed_slack_notification
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Generate blog post',
            status: :failed,
            release_type: :security
          ).send_notification
        end

        def send_success_slack_notification(blog_mr_url)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Generate blog post',
            status: :success,
            release_type: :security,
            extra_string: "Blog MR: #{blog_mr_url}"
          ).send_notification
        end

        def coordinator
          ReleaseTools::PatchRelease::Coordinator.new
        end

        def failure_message
          <<~MSG
            Generating security release blog post failed. If this job continues to fail,
            the blog post can be created by executing the `release:patch_blog_post` rake task.
          MSG
        end
      end
    end
  end
end
