# frozen_string_literal: true

module ReleaseTools
  module Security
    class SecurityMirrorNotificationService
      include ::SemanticLogger::Loggable

      RepositoriesOutOfSync = Class.new(StandardError)

      def execute
        post_general_status

        return unless security_release_pipeline?

        logger.info('Running as part of a security pipeline')

        post_job_status

        raise RepositoriesOutOfSync unless mirror_message.synced_repositories?
      end

      private

      def security_mirrors
        @security_mirrors ||= client
          .group_projects('gitlab-org/security', include_subgroups: true)
          .auto_paginate
          .map(&:to_h)
          .select { |p| p.key?('forked_from_project') }
          .sort_by { |p| p['path'] }
          .map { |p| ReleaseTools::Security::MirrorStatus.new(p) }
          .select(&:available?)
      end

      def mirror_message
        ReleaseTools::Slack::Security::MirrorMessage.new(
          security_mirrors: security_mirrors
        )
      end

      def post_general_status
        mirror_message.general_status
      end

      def post_job_status
        mirror_message.job_status
      end

      def security_release_pipeline?
        ENV.fetch('SECURITY_RELEASE_PIPELINE', nil)
      end

      def client
        @client ||= ReleaseTools::GitlabClient
      end
    end
  end
end
