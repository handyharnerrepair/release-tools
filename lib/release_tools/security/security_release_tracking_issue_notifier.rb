# frozen_string_literal: true

module ReleaseTools
  module Security
    class SecurityReleaseTrackingIssueNotifier
      include ReleaseTools::Security::IssueHelper
      include ::SemanticLogger::Loggable

      def initialize(issue)
        @issue = issue
      end

      def self.notify(issue)
        new(issue).execute
      end

      def execute
        return unless ManagedVersioning::PROJECTS_NEEDING_MANUAL_RELEASES.map(&:security_id).include?(issue.project_id)

        logger.info('Notifying RMs that issue was linked to security release tracking issue', linked_issue: issue.web_url, security_tracking_issue: security_tracking_issue.web_url)

        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          client.create_issue_note(
            security_tracking_issue.project_id,
            issue: security_tracking_issue,
            body: notification_message
          )
        end
      end

      private

      attr_reader :issue

      def client
        ReleaseTools::GitlabClient
      end

      def notification_message
        <<~MSG
          #{assignees_string}, a managed versioning project issue, #{issue.web_url}, has been linked to this security release. Please
          [follow the release manager instructions](https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/security_release.md#release-manager-process)
          and adjust the [security release task issue](#{security_task_issue.web_url}) to include any additional steps needed.
        MSG
      end

      def assignees_string
        return "@gitlab-org/release/managers" if issue.assignees.empty?

        issue.assignees.map do |assignee|
          "@#{assignee.username}"
        end.join(', ')
      end
    end
  end
end
