# frozen_string_literal: true

module ReleaseTools
  module Security
    class SyncGitRemotesService
      include ReleaseTools::Services::SyncRefsHelper
      include ::SemanticLogger::Loggable

      def initialize(git_versions = [])
        @git_versions = git_versions
      end

      def execute
        sync_all_tags
      end

      def sync_all_tags
        sync_tags(Project::Git, *@git_versions)
      end
    end
  end
end
