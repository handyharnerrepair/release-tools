# frozen_string_literal: true

require 'date'

module ReleaseTools
  module Security
    class TrackingIssue < ReleaseTools::Issue
      include ::SemanticLogger::Loggable

      # Represent the cadence to schedule planned security releases
      RELEASE_CADENCE = 2.weeks

      def title
        prefix =
          if SharedStatus.critical_security_release?
            'Critical security'
          else
            'Security'
          end

        "#{prefix} release: #{versions_title}"
      end

      def confidential?
        true
      end

      def labels
        'upcoming security release,security,meta'
      end

      def project
        ReleaseTools::Project::GitlabEe
      end

      def versions_title
        next_security_release_versions.map! do |version|
          version.sub(/\d+$/, 'x')
        end.join(', ')
      end

      # Regular Security Releases are performed in the next milestone of the
      # last version released, e.g if the last version released is 13.6, the
      # Security Release will be performed on %13.7.
      #
      # To be deprecated by https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1125
      def version
        next_version
      end

      def due_date
        nil # To be defined on https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/19797
      end

      def assignees
        release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      protected

      def template_path
        File.expand_path('../../../templates/security_release_tracking_issue.md.erb', __dir__)
      end

      def next_security_release_versions
        if multiple_security_releases?
          logger.info("multiple_security_releases enabled, fetching the active versions for #{RELEASE_CADENCE.from_now}")

          upcoming_versions_for_release_cadence
        else
          logger.info('multiple_security_releases disabled, fetching the versions after the monthly release', next_version: next_version)

          upcoming_versions_for_the_monthly_release
        end
      end

      def upcoming_versions_for_release_cadence
        minor_for_date = gitlab_releases_client
          .current_minor_for_date(RELEASE_CADENCE.from_now)

        gitlab_releases_client
          .previous_minors(minor_for_date)
          .map { |version| "#{version}.x" }
      end

      def next_version
        @next_version ||= ReleaseTools::Version.new(ReleaseTools::ProductMilestone.current.title)
      end

      def upcoming_versions_for_the_monthly_release
        [
          next_version,
          ReleaseTools::Versions.next_versions.take(2)
        ].flatten
      end

      def release_managers
        if multiple_security_releases?
          scheduled_release_managers
        else
          release_managers_schedule.active_release_managers
        end
      end

      def scheduled_release_managers
        active_version_for_date = gitlab_releases_client
          .version_for_date(RELEASE_CADENCE.from_now)

        version = ReleaseTools::Version.new(active_version_for_date)

        release_managers_schedule.authorized_release_managers(version)
      end

      def gitlab_releases_client
        ReleaseTools::GitlabReleasesClient
      end

      def release_managers_schedule
        @release_managers_schedule ||= ReleaseManagers::Schedule.new
      end

      def multiple_security_releases?
        Feature.enabled?(:multiple_security_releases)
      end
    end
  end
end
