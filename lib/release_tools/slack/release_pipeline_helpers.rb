# frozen_string_literal: true

module ReleaseTools
  module Slack
    module ReleasePipelineHelpers
      EMOJI = {
        security: ":security-tanuki:",
        monthly: ":release:"
      }.freeze
    end
  end
end
