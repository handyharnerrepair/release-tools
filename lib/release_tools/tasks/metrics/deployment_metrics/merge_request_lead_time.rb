# frozen_string_literal: true

require 'release_tools/deployment_util'

module ReleaseTools
  module Tasks
    module Metrics
      module DeploymentMetrics
        # Publish metrics MR lead time
        class MergeRequestLeadTime
          include ::SemanticLogger::Loggable
          include ReleaseTools::DeploymentUtil

          def execute(env, version)
            product_version = ProductVersion.from_package_version(version)
            if product_version.nil?
              logger.warn("Product version not found", version: version)
              return nil
            end

            sha = product_version[Project::GitlabEe.metadata_project_name].sha

            # We are interested on successful deployments only
            deployments = find_deployments(env, sha, 'success')

            if deployments.empty?
              logger.warn("Deployments not found", product_version: product_version.version)
              return nil
            end

            deployments.each do |deployment|
              merge_requests = get_merge_requests(Project::GitlabEe, deployment.id)
              env, stage = parse_env(deployment.environment.name)

              merge_requests.each do |mr|
                deployed_at = Time.parse(deployment.updated_at)
                merged_at = Time.parse(mr.merged_at)

                lead_time = deployed_at.to_i - merged_at.to_i
                adjusted_lead_time = get_adjusted_lead_time(deployed_at, merged_at)

                logger.info(
                  "Recorded MR lead time metric",
                  environment: env,
                  stage: stage,
                  id: mr.iid,
                  title: mr.title,
                  web_url: mr.web_url,
                  version: version
                )

                next if SharedStatus.dry_run?

                metric.set("deployment_merge_request_lead_time_seconds", lead_time, labels: "#{env},#{stage},#{deployment.iid},#{mr.iid},#{version}")
                metric.set("deployment_merge_request_adjusted_lead_time_seconds", adjusted_lead_time, labels: "#{env},#{stage},#{deployment.iid},#{mr.iid},#{version}")
              end
            end
          end

          private

          def gitlab_client
            ReleaseTools::GitlabClient
          end

          def metric
            @metric ||= ReleaseTools::Metrics::Client.new
          end

          def parse_env(environment)
            return [environment, 'main'] unless environment.end_with?('-cny')

            env = environment.delete_suffix('-cny')
            [env, 'cny']
          end

          # Count the number of seconds from the time when MR was merged to the time it was deployed.
          # Weekends are not counted. Timestamps are all in UTC, so weekend is considered to start
          # at 00:00 UTC on Saturday and ends at 00:00 UTC on Monday.
          # Let's use an example to see how this method works. Consider an MR was merged
          # at 12:00 UTC on Thursday and deployed at 12:00 UTC on next Tuesday.
          # - First count the number of full days between merge and deploy. In the example, it would be
          #   Friday and Monday, so 2 days.
          # - Then count the seconds on Thursday from the time the MR was merged, from 12:00 UTC to end of day.
          # - Then count the seconds on Tuesday from start of day to 12:00 UTC, when the MR was deployed.
          def get_adjusted_lead_time(deployed_at_time, merged_at_time)
            merged_at_day = merged_at_time.to_date
            deployed_at_day = deployed_at_time.to_date

            seconds_count = 0

            if merged_at_day == deployed_at_day
              seconds_count = deployed_at_time.to_i - merged_at_time.to_i

            else
              seconds_count = business_days_from_merge_to_deploy(merged_at_day, deployed_at_day).days

              # Count the seconds from the time when MR was merged to the end of that day.
              seconds_count += merged_at_time.seconds_until_end_of_day.to_i if merged_at_time.on_weekday?

              # Count the seconds from start of the day to the time when MR was deployed.
              seconds_count += deployed_at_time.seconds_since_midnight.to_i
            end

            seconds_count
          end

          def business_days_from_merge_to_deploy(merged_at_day, deployed_at_day)
            # Count the weekdays between the day when MR was merged and the day when it was deployed.
            # This count needs to exclude the merged_at_day and the deployed_at_day, since those days
            # are handled separately.
            ((merged_at_day + 1.day)...deployed_at_day).count(&:on_weekday?)
          end

          def get_merge_requests(project_id, deployment_id)
            gitlab_client.deployed_merge_requests(project_id, deployment_id).auto_paginate
          end

          def find_deployments(env, sha, status)
            canonical_sha = auto_deploy_intersection(ReleaseTools::Project::GitlabEe, sha)
            canonical  = gitlab_client.deployments(ReleaseTools::Project::GitlabEe, env, status: status).detect { |dep| dep.sha == canonical_sha }

            security = gitlab_client.deployments(ReleaseTools::Project::GitlabEe.security_path, env, status: status).detect { |dep| dep.sha == sha }

            [security, canonical].compact
          end
        end
      end
    end
  end
end
