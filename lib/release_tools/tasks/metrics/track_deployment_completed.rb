# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Metrics
      # Publish metrics related to number of completed deployments
      class TrackDeploymentCompleted
        def execute
          client.inc("deployment_completed_total", labels: environment)

          client.set("deployment_completed", 1, labels: labels)
        end

        private

        def client
          @client ||= ReleaseTools::Metrics::Client.new
        end

        def environment
          ENV.fetch("DEPLOY_ENVIRONMENT")
        end

        def deploy_version
          ENV.fetch("DEPLOY_VERSION")
        end

        def labels
          "#{environment},#{deploy_version}"
        end
      end
    end
  end
end
