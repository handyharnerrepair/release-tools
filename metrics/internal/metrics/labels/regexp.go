package labels

import (
	"regexp"
)

var minorVersion = regexp.MustCompile(`^\d+.\d+$`)
var integer = regexp.MustCompile(`^\d+$`)
var fullDeployVersion = regexp.MustCompile(`^\d+\.\d+\.\d+-\b[a-f0-9]{11}\.\b[a-f0-9]{11}$`)
var anyStringRegexp = regexp.MustCompile(`.*`)
var webURL = regexp.MustCompile(`^https://dev\.gitlab\.org|ops\.gitlab\.net|gitlab\.com/.+/-/.+/\d+$`)

func FromRegexp(name string, r *regexp.Regexp) *regularExpression {
	return &regularExpression{
		base:    &base{name: name},
		matcher: r,
	}
}

func MinorVersion(name string) *regularExpression {
	return FromRegexp(name, minorVersion)
}

func FullDeployVersion(name string) *regularExpression {
	return FromRegexp(name, fullDeployVersion)
}

func AnyString(name string) *regularExpression {
	return FromRegexp(name, anyStringRegexp)
}

func WebURL(name string) *regularExpression {
	return FromRegexp(name, webURL)
}

func Integer(name string) *regularExpression {
	return FromRegexp(name, integer)
}

type regularExpression struct {
	matcher *regexp.Regexp

	*base
}

func (r *regularExpression) Values() []string {
	return nil
}

func (r *regularExpression) CheckValue(value string) bool {
	return r.matcher.MatchString(value)
}
