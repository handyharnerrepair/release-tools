package metrics

import (
	"testing"

	"gitlab.com/gitlab-org/release-tools/metrics/internal/metrics/labels"
)

func TestCheckLabels(t *testing.T) {
	labelNames := []string{"color", "level"}
	allowedValues := [][]string{
		{"green", "yellow", "red"},
		{"high", "medium", "low"},
	}

	examples := []struct {
		name          string
		values        []string
		labels        []string
		allowedValues [][]string
		error         string
	}{
		{
			name:          "1 label ok",
			values:        []string{"yellow"},
			labels:        labelNames[0:1],
			allowedValues: allowedValues[0:1],
		},
		{
			name:          "1 label ko",
			values:        []string{"not_a_color"},
			labels:        labelNames[0:1],
			allowedValues: allowedValues[0:1],
			error:         `"not_a_color" is not a valid "color" value`,
		},
		{
			name:          "2 labels ok",
			values:        []string{"yellow", "high"},
			labels:        labelNames,
			allowedValues: allowedValues,
		},
		{
			name:          "2 labels - first ko",
			values:        []string{"not_a_color", "high"},
			labels:        labelNames,
			allowedValues: allowedValues,
			error:         `"not_a_color" is not a valid "color" value`,
		},
		{
			name:          "2 labels - second ko",
			values:        []string{"red", "foo"},
			labels:        labelNames,
			allowedValues: allowedValues,
			error:         `"foo" is not a valid "level" value`,
		},
		{
			name:          "missing label values",
			values:        []string{"red"},
			labels:        labelNames,
			allowedValues: allowedValues,
			error:         `Expected 2 labels`,
		},
		{
			name:          "too many label values",
			values:        []string{"red", "high", "extra"},
			labels:        labelNames,
			allowedValues: allowedValues,
			error:         `Expected 2 labels`,
		},
	}

	for _, example := range examples {
		t.Run(example.name, func(tt *testing.T) {
			labelDefinitions := make([]Label, len(example.labels))

			for i, label := range example.labels {
				labelDefinitions[i] = labels.FromValues(
					label,
					example.allowedValues[i],
				)
			}
			meta := description{labels: labelDefinitions}
			err := meta.CheckLabels(example.values)

			if example.error == "" && err == nil {
				return
			}

			if example.error == "" && err != nil {
				tt.Fatalf("Unexpected error, got: %q", err)
			}

			if err == nil {
				tt.Fatalf("Expected error %q, got nil", example.error)
			}

			if err.Error() != example.error {
				tt.Fatalf("Expected error %q, got %q", example.error, err)
			}
		})
	}
}

func TestWithCarsesianProductLabelReset(t *testing.T) {
	examples := []struct {
		name           string
		opts           []MetricOption
		expectedValues [][]string
	}{
		{
			name:           "no labels",
			expectedValues: nil,
		},
		{
			name: "1 label",
			opts: []MetricOption{WithLabel(labels.FromValues("severity", []string{"low", "high"}))},
			expectedValues: [][]string{
				{"low"},
				{"high"},
			},
		},
		{
			name: "2 labels",
			opts: []MetricOption{
				WithLabel(labels.FromValues("severity", []string{"low", "high"})),
				WithLabel(labels.FromValues("environment", []string{"staging", "production"})),
			},
			expectedValues: [][]string{
				{"low", "staging"},
				{"low", "production"},
				{"high", "staging"},
				{"high", "production"},
			},
		},
		{
			name: "3 labels",
			opts: []MetricOption{
				WithLabel(labels.FromValues("severity", []string{"low", "high"})),
				WithLabel(labels.FromValues("environment", []string{"staging", "production"})),
				WithLabel(labels.FromValues("service", []string{"test"})),
			},
			expectedValues: [][]string{
				{"low", "staging", "test"},
				{"low", "production", "test"},
				{"high", "staging", "test"},
				{"high", "production", "test"},
			},
		},
	}

	for _, example := range examples {
		t.Run(example.name, func(tt *testing.T) {
			example.opts = append(example.opts, WithCartesianProductLabelReset())
			descOpts := applyMetricOptions(example.opts)

			if len(descOpts.labelsToInitialize) != len(example.expectedValues) {
				tt.Fatalf("Expected %v label set, got %v", len(example.expectedValues), len(descOpts.labelsToInitialize))
			}

			for i, labels := range descOpts.labelsToInitialize {
				expected := example.expectedValues[i]
				if len(labels) != len(expected) {
					tt.Errorf("At index %v expected %v label values, got %v", i, len(expected), len(labels))
				}

				for j, labelValue := range labels {
					if labelValue != expected[j] {
						tt.Errorf("At index %v expected %q, got %q", j, expected[j], labelValue)
					}
				}
			}
		})
	}
}
