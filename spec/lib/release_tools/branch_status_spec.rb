# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::BranchStatus do
  let(:versions) { %w[1.1.0 1.2.1 1.3.4] }
  let(:dev_client) { stub_const('ReleaseTools::GitlabDevClient', spy) }
  let(:gitlab_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:project) { ReleaseTools::Project::GitlabEe }

  subject(:branch_status) { described_class }

  describe '.for_security_release' do
    it 'supplies the latest security versions' do
      mapped = versions.map { |v| ReleaseTools::Version.new(v) }

      expect(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(versions)

      expect(described_class)
        .to receive(:for)
        .with(mapped)

      branch_status.for_security_release
    end

    it 'fetches stable branches on security repo' do
      allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(versions)

      expect(gitlab_client)
        .to receive(:pipelines)
        .with(project.security_path, { ref: '1-1-stable-ee', per_page: 1 })
        .once

      branch_status.for_security_release
    end
  end

  describe '.for' do
    it 'returns a `project => status` Hash' do
      allow(described_class).to receive(:project_pipeline).and_return(true)

      results = described_class.for(versions)

      expect(results.keys).to eq(described_class::PROJECTS)
      expect(results.values.count).to eq(versions.count)
      expect(results.values.flatten.count).to eq(described_class::PROJECTS.count * versions.count)
    end

    it 'fetches rc packages on dev' do
      allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(versions)

      expect(dev_client)
        .to receive(:pipelines)
        .with(project, { ref: 'v1.1.0-rc42-ee', per_page: 1 })
        .once

      branch_status.for([ReleaseTools::Version.new('1.1.0-rc42')])
    end
  end
end
