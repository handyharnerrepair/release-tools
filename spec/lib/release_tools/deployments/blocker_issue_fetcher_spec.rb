# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockerIssueFetcher do
  let(:release_issues) do
    [
      create(:issue, labels: ['release-blocker']),
      create(:issue, labels: ['release-blocker']),
      create(:issue, labels: ['release-blocker']),
      create(:issue, labels: %w(release-blocker dev-failure))
    ]
  end

  let(:incidents) do
    [
      create(:issue, labels: ['Deploys-blocked-gstg::6hr']),
      create(:issue, labels: ['Deploys-blocked-gprd::10hr']),
      create(:issue, labels: ['Deploys-blocked-gprd::10hr']),
      create(:issue, labels: ['Service::Kube']),
      create(:issue, labels: ['Source::IMA::IncidentDeclare'])
    ]
  end

  subject(:blockers) { described_class.new }

  before do
    allow(blockers)
      .to receive(:issues)
      .and_return(release_issues)

    allow(blockers)
      .to receive(:incidents)
      .and_return(incidents)
  end

  describe '#deployment_blockers' do
    it 'lists release and incidents with blocked-deploy labels' do
      blockers.fetch

      deployment_blockers = blockers.deployment_blockers
      labels = labels_for(deployment_blockers)

      expect(deployment_blockers.count).to eq(6)

      expect(labels)
        .to match_array(%w(release-blocker Deploys-blocked-gstg::6hr Deploys-blocked-gprd::10hr))
    end

    it 'removes dev failures' do
      blockers.fetch

      labels = labels_for(blockers.deployment_blockers)

      expect(labels).not_to include('dev-failure')
    end
  end

  describe '#uncategorized_incidents' do
    it 'lists incidents with no deploys-blocked labels' do
      blockers.fetch

      incidents = blockers.uncategorized_incidents
      labels = labels_for(incidents)

      expect(incidents.count).to eq(2)
      expect(labels).to match_array(%w(Service::Kube Source::IMA::IncidentDeclare))
    end
  end

  def labels_for(issues)
    issues
      .map { |issue| issue.data.labels }
      .flatten
      .uniq
  end
end
