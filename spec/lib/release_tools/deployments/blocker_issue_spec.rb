# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockerIssue do
  let(:labels) do
    [
      'Deploys-blocked-gprd::4hr',
      'Deploys-blocked-gstg::0.5hr',
      'incident',
      'Incident::Resolved',
      'Service::Kube',
      'Source::IMA::IncidentDeclare',
      'severity::3',
      'RootCause::Software-Change'
    ]
  end

  let(:issue) do
    create(
      :issue,
      title: 'incident blocker',
      labels: labels
    )
  end

  subject(:blocker) do
    described_class.new(issue)
  end

  describe '#title' do
    it 'returns an expandable web_url' do
      expect(blocker.title).to eq("#{issue.web_url}+")
    end
  end

  describe '#hours_gstg_blocked' do
    context 'with deploy block label' do
      it 'fetches the hours from the gstg label' do
        expect(blocker.hours_gstg_blocked).to eq(0.5)
      end
    end

    context 'with no deploy block label' do
      let(:labels) { ['incident'] }

      it { expect(blocker.hours_gstg_blocked).to eq(0) }
    end
  end

  describe '#hours_gprd_blocked' do
    context 'with deploy block label' do
      it 'fetches the hours from the gprd label' do
        expect(blocker.hours_gprd_blocked).to eq(4)
      end
    end

    context 'with no deploy block label' do
      let(:labels) { ['incident'] }

      it { expect(blocker.hours_gstg_blocked).to eq(0) }
    end
  end

  describe '#root_cause' do
    context 'when the labels include a root cause' do
      it 'returns the root cause label' do
        expect(blocker.root_cause).to eq("~\"RootCause::Software-Change\"")
      end
    end

    context 'when the labels do not include a root cause label' do
      let(:labels) do
        [
          'Deploys-blocked-gprd::4hr',
          'Deploys-blocked-gstg::0.5hr',
          'incident',
          'Incident::Resolved',
          'severity::3'
        ]
      end

      it 'returns nothing' do
        expect(blocker.root_cause).to be_nil
      end
    end
  end
end
