# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::GitlabReleasesClient do
  subject(:client) { described_class }

  describe '.active_version' do
    it 'calls releases method' do
      expect(GitlabReleases).to receive(:active_version)

      client.active_version
    end
  end

  describe '.version_for_date' do
    it 'calls a releases method' do
      expect(GitlabReleases).to receive(:version_for_date)

      client.version_for_date(Date.today)
    end
  end

  describe '.upcoming_releases' do
    it 'calls a releases method' do
      expect(GitlabReleases).to receive(:upcoming_releases)

      client.upcoming_releases
    end
  end
end
