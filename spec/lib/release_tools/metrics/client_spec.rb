# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::Client do
  let(:client) { instance_double(HTTP::Client) }

  before do
    allow(HTTP).to receive(:headers).and_return(client)
  end

  describe '#reset' do
    it 'sends a DELETE request' do
      metric = 'my_metric'

      expect(client).to receive(:delete)
                          .with("#{described_class::GATEWAY}/api/#{metric}")
                          .and_return(double(:response, status: HTTP::Response::Status.new(200)))

      subject.reset(metric)
    end
  end
end
