# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::WWWGitlabCom do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .security_group', 'gitlab-org/security'
  it_behaves_like 'project .security_path', 'gitlab-org/security/www-gitlab-com'
  it_behaves_like 'project .to_s'

  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-com/www-gitlab-com' }
  end

  describe '.dev_path' do
    it { expect(described_class.dev_path).to eq 'gitlab/www-gitlab-com' }
  end

  describe '.group' do
    it { expect(described_class.group).to eq 'gitlab-com' }
  end

  describe '.dev_group' do
    it { expect(described_class.dev_group).to eq 'gitlab' }
  end
end
