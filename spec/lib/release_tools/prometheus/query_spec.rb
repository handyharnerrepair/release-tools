# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Prometheus::Query do
  subject(:prom_query) { described_class.new }

  before do
    allow(HTTP)
      .to receive(:get)
      .with(described_class::ENDPOINT, anything)
      .and_return(
        instance_spy(
          HTTP::Response,
          status: HTTP::Response::Status.new(200),
          parse: json_response
        )
      )
  end

  describe '#run' do
    subject(:run_query) { prom_query.run(query) }

    let(:json_response) { {} }
    let(:query) { 'gitlab_deployment_health:service' }

    it 'returns parsed response' do
      expect(run_query).to eq(json_response)
    end

    context 'with connection error' do
      before do
        allow(HTTP).to receive(:get).and_raise(HTTP::ConnectionError)
      end

      it 'retries and raises error on repeated failure' do
        expect(HTTP).to receive(:get).at_least(3).times

        expect { run_query }.to raise_error(HTTP::ConnectionError)
      end
    end

    context 'with error response' do
      before do
        allow(HTTP)
          .to receive(:get)
          .and_return(
            instance_spy(
              HTTP::Response,
              status: HTTP::Response::Status.new(400)
            )
          )
      end

      it 'retries and raises error on repeated failure' do
        expect(HTTP).to receive(:get).at_least(3).times

        expect { run_query }.to raise_error(described_class::PromQLError)
      end
    end
  end
end
