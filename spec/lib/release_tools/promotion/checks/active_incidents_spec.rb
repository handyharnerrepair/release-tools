# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::Checks::ActiveIncidents do
  subject(:check) { described_class.new(scope: scope) }

  let(:scope) { :deployment }

  describe '#name' do
    it 'includes blocking label references' do
      expect(check.name).to eq('active ~"blocks deployments" incidents')
    end
  end

  describe '#fine?' do
    def stub_issues(*issues)
      allow(ReleaseTools::GitlabClient).to receive(:issues).and_return(issues)
    end

    shared_examples 'is fine' do |labels|
      it 'is fine with labels' do
        stub_issues(create(:issue, labels: ['Incident::Active'] + Array(labels)))

        expect(check).to be_fine
      end
    end

    shared_examples 'is not fine' do |labels|
      it 'is fine with labels' do
        stub_issues(create(:issue, labels: ['Incident::Active'] + Array(labels)))

        expect(check).not_to be_fine
      end
    end

    it_behaves_like 'is fine', described_class::FEATURE_FLAG_BLOCKING_LABEL

    it 'returns true with no incidents' do
      stub_issues

      expect(check).to be_fine
    end

    it_behaves_like 'is not fine', described_class::DEPLOYMENT_BLOCKING_LABEL
    it_behaves_like 'is fine', 'severity::1'

    context 'with feature_flag as scope' do
      let(:scope) { :feature_flag }

      it 'is fine with no incidents' do
        stub_issues

        expect(check).to be_fine
      end

      it_behaves_like 'is fine', 'severity::1'
      it_behaves_like 'is not fine', described_class::FEATURE_FLAG_BLOCKING_LABEL
      it_behaves_like 'is fine', described_class::DEPLOYMENT_BLOCKING_LABEL
    end
  end
end
