# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PublicRelease::GitlabOperatorRelease do
  let(:version) { ReleaseTools::Version.new('0.7.0') }
  let(:chart_version) { ReleaseTools::Version.new('6.0.0') }
  let(:gitlab_version) { ReleaseTools::Version.new('2.0.0-ee') }
  let(:release) { described_class.new(version, chart_version, gitlab_version) }
  let(:finder) { ReleaseTools::Helm::HelmVersionFinder.new }
  let(:chart_versions) do
    [
      ReleaseTools::Version.new('6.0.0'),
      ReleaseTools::Version.new('5.4.3'),
      ReleaseTools::Version.new('2.1.0')
    ]
  end

  before do
    allow(::ReleaseTools::Helm::HelmVersionFinder).to receive(:new).and_return(finder)
    allow(finder).to receive(:latest_version).and_return(ReleaseTools::Version.new('6.0.0'))
    allow(finder).to receive(:latest_minor_versions).and_return(chart_versions)
  end

  describe '#execute' do
    context 'when releasing a Helm RC' do
      let(:gitlab_version) { ReleaseTools::Version.new('2.0.0-rc42-ee') }

      it 'does nothing' do
        expect(release).not_to receive(:create_target_branch)

        release.execute
      end
    end

    context 'when releasing a latest stable release' do
      it 'performs the release' do
        # When SLACK_TAG_URL is defined, a call is made to the URL.
        # Stubbing `notify_slack` prevents this call.
        allow(release).to receive(:notify_slack)

        tag = double(:tag, name: '0.7.0')

        expect(release).to receive(:create_target_branch)
        expect(release).to receive(:compile_changelog)
        expect(release).to receive(:update_chart_files)
        expect(release).to receive(:update_chart_yml_file)
        expect(release).to receive(:create_tag).and_return(tag)
        expect(release).to receive(:wait_for_tag_in_dev)
        expect(release).to receive(:sync_branch_and_tag)

        # The SLACK_TAG_URL is set here in order to ensure consistent
        # behavior of this spec in ops and .com. Otherwise, SLACK_TAG_URL
        # is only defined in ops (as a CI/CD project variable), and the spec
        # behaves differently on ops and .com.
        ClimateControl.modify(SLACK_TAG_URL: 'http://gitlab.com') do
          without_dry_run do
            release.execute
          end
        end
      end
    end

    context 'when releasing a backport Helm version' do
      let(:chart_version) { ReleaseTools::Version.new('5.4.4') }

      it 'does nothing' do
        expect(release).not_to receive(:create_target_branch)

        release.execute
      end
    end

    context 'with a dry_run' do
      it 'does nothing' do
        expect(release).not_to receive(:create_target_branch)
        expect(release).not_to receive(:compile_changelog)
        expect(release).not_to receive(:update_chart_files)
        expect(release).not_to receive(:update_chart_yml_file)
        expect(release).not_to receive(:create_tag)
        expect(release).not_to receive(:wait_for_tag_in_dev)
        expect(release).not_to receive(:sync_branch_and_tag)

        release.execute
      end
    end
  end

  describe '#compile_changelog' do
    it 'compiles the changelog' do
      compiler = instance_spy(ReleaseTools::ChangelogCompiler)

      expect(ReleaseTools::ChangelogCompiler)
        .to receive(:new)
        .with(release.project_path, { client: release.client })
        .and_return(compiler)

      expect(compiler)
        .to receive(:compile)
        .with(version, { branch: release.target_branch })

      release.compile_changelog
    end
  end

  describe '#update_chart_files' do
    it 'updates chart_versions file in both target branch and default branch' do
      expect(release).to receive(:commit_version_files).with(
        release.target_branch,
        { 'CHART_VERSIONS' => "6.0.0\n5.4.3\n2.1.0" },
        { message: "Update CHART_VERSIONS for GitLab Chart release 6.0.0" }
      )

      expect(release).to receive(:commit_version_files).with(
        ReleaseTools::Project::GitlabOperator.default_branch,
        { 'CHART_VERSIONS' => "6.0.0\n5.4.3\n2.1.0" },
        { message: "Update CHART_VERSIONS for GitLab Chart release 6.0.0" }
      )

      release.update_chart_files(%w[6.0.0 5.4.3 2.1.0])
    end
  end

  describe '#update_chart_yml_file' do
    it 'updates Chart.yml file with incoming version' do
      path = 'deploy/chart/Chart.yaml'

      old_yaml = YAML.dump(
        'name' => 'gitlab-operator',
        'version' => '0.6.3',
        'appVersion' => '0.6.3'
      )

      new_yaml = YAML.dump(
        'name' => 'gitlab-operator',
        'version' => '0.7.0',
        'appVersion' => '0.7.0'
      )

      expect(release).to receive(:read_file).with(path).and_return(old_yaml)
      expect(release).to receive(:commit_version_files).with(
        release.target_branch,
        { path => new_yaml },
        { message: "Update Chart version to 0.7.0\n\n [ci skip]" }
      )

      expect(release).to receive(:commit_version_files).with(
        ReleaseTools::Project::GitlabOperator.default_branch,
        { path => new_yaml },
        { message: "Update Chart version to 0.7.0\n\n [ci skip]" }
      )

      release.update_chart_yml_file
    end
  end

  describe '#create_tag' do
    it 'creates the tag' do
      expect(release.client)
        .to receive(:find_or_create_tag)
        .with(
          release.project_path,
          version,
          release.target_branch,
          { message: 'Version 0.7.0 - supports GitLab Charts 6.0.0, 5.4.3, 2.1.0' }
        )

      release.create_tag(%w[6.0.0 5.4.3 2.1.0])
    end
  end

  describe '#wait_for_tag_in_dev' do
    let(:fake_repo) do
      instance_double(ReleaseTools::RemoteRepository, fetch: false).as_null_object
    end

    before do
      allow(ReleaseTools::RemoteRepository).to receive(:get)
        .with(
          a_hash_including(release.project::REMOTES),
          a_hash_including(branch: '0-7-stable')
        ).and_return(fake_repo)
    end

    it 'returns when tag is found in Build mirror' do
      allow(fake_repo).to receive(:fetch).with("refs/tags/0.7.0", { remote: :dev }).and_return(true)

      expect { release.wait_for_tag_in_dev }.not_to raise_error
    end

    it 'raises when the tag is not found in Build mirror' do
      allow(fake_repo).to receive(:fetch).with("refs/tags/0.7.0", { remote: :dev }).and_return(false)

      expect { release.wait_for_tag_in_dev }.to raise_error(described_class::TagNotFoundInBuildMirror)
    end

    it 'retries the operation when the tag is not found in Build mirror' do
      allow(fake_repo).to receive(:fetch).with("refs/tags/0.7.0", { remote: :dev }).twice.and_return(false, true)

      expect { release.wait_for_tag_in_dev }.not_to raise_error
    end
  end

  describe '#sync_branch_and_tag' do
    before do
      allow(release).to receive(:sync_branches).and_return(true)
      allow(release).to receive(:sync_tags).and_return(true)
    end

    it 'syncs the branch' do
      expect(release).to receive(:sync_branches).with(release.project, '0-7-stable')

      release.sync_branch_and_tag
    end

    it 'syncs the tag' do
      expect(release).to receive(:sync_tags).with(release.project, '0.7.0')

      release.sync_branch_and_tag
    end
  end

  describe '#project' do
    it 'returns the project' do
      expect(release.project).to eq(ReleaseTools::Project::GitlabOperator)
    end
  end

  describe '#source_for_target_branch' do
    context 'when a custom commit is specified' do
      it 'returns the commit' do
        release = described_class.new(version, chart_version, gitlab_version, commit: 'foo')

        expect(release.source_for_target_branch).to eq('foo')
      end
    end

    context 'when no custom commit is specified' do
      it 'returns the default branch name' do
        release = described_class.new(version, chart_version, gitlab_version)

        expect(release.source_for_target_branch).to eq(described_class::DEFAULT_BRANCH)
      end
    end
  end
end
