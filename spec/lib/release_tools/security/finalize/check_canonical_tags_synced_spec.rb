# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::CheckCanonicalTagsSynced do
  let(:versions) do
    [
      ReleaseTools::Version.new('16.1.3'),
      ReleaseTools::Version.new('16.0.8'),
      ReleaseTools::Version.new('15.11.13')
    ]
  end

  let(:client_tags) do
    [
      create(:tag, name: 'v16.1.2-ee'),
      create(:tag, name: 'v16.0.7-ee'),
      create(:tag, name: 'v15.11.12-ee'),
      create(:tag, name: 'v15.10.10-ee')
    ]
  end

  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:slack_notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }

  subject(:verifier) { described_class.new }

  before do
    allow(ReleaseTools::Versions)
      .to receive(:next_versions)
      .and_return(versions)
  end

  describe '#execute' do
    it 'checks if the synced tags exist on canonical' do
      expect(client)
        .to receive(:tags)
        .with(
          ReleaseTools::Project::GitlabEe,
          order_by: 'updated',
          sort: 'desc'
        ).and_return(client_tags)

      expect(slack_notifier)
        .to receive(:new)
        .with(job_type: 'Check canonical tags', status: :success, release_type: :security)

      verifier.execute
    end

    context 'when tags are not synced' do
      let(:client_tags) do
        [
          create(:tag, name: 'v16.1.2-ee'),
          create(:tag, name: 'v15.11.12-ee'),
          create(:tag, name: 'v15.10.10-ee')
        ]
      end

      it 'raises an error' do
        expect(client)
          .to receive(:tags)
          .with(
            ReleaseTools::Project::GitlabEe,
            order_by: 'updated',
            sort: 'desc'
          ).and_return(client_tags)

        expect(slack_notifier)
          .to receive(:new)
          .with(job_type: 'Check canonical tags', status: :failed, release_type: :security)

        expect { verifier.execute }
          .to raise_error(described_class::CouldNotCompleteError)
      end
    end

    context 'when an error occurs' do
      it 'raises an error' do
        expect(client)
          .to receive(:tags)
          .and_raise(StandardError)

        expect(slack_notifier)
          .to receive(:new)
          .with(job_type: 'Check canonical tags', status: :failed, release_type: :security)

        expect { verifier.execute }
          .to raise_error(described_class::CouldNotCompleteError)
      end
    end
  end
end
