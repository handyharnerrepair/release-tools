# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::IssueHelper do
  before do
    foo_class = Class.new do
      include ReleaseTools::Security::IssueHelper
    end

    stub_const('FooClass', foo_class)
  end

  let(:foo_class) { FooClass.new }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }

  describe '#security_tracking_issue' do
    it 'fetches the security tracking issue' do
      expect(client).to receive(:next_security_tracking_issue)

      foo_class.security_tracking_issue
    end
  end

  describe '#security_task_issue' do
    it 'fetches the security task issue' do
      expect(client).to receive(:current_security_task_issue)

      foo_class.security_task_issue
    end
  end

  describe '#reference_issue' do
    before do
      allow(client).to receive(:next_security_tracking_issue).and_return('tracking issue')
      allow(client).to receive(:current_security_task_issue).and_return('task issue')
    end

    it 'returns the tracking issue' do
      expect(foo_class.reference_issue).to eq('tracking issue')
    end

    context 'critical security release' do
      it 'returns the task issue' do
        ClimateControl.modify(SECURITY: 'critical') do
          expect(foo_class.reference_issue).to eq('task issue')
        end
      end
    end
  end
end
