# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Prepare::OmnibusNightly do
  let(:client) { stub_const('ReleaseTools::GitlabDevClient', spy) }
  let(:project) { ReleaseTools::Project::OmnibusGitlab }
  let(:action) { :disable }

  let(:notifier) do
    stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy)
  end

  let(:pipeline_schedules) do
    [
      create(:pipeline_schedule, id: 1, description: 'EE nightly'),
      create(:pipeline_schedule, id: 2, description: 'CE nightly'),
      create(:pipeline_schedule, id: 3, description: 'foo')
    ]
  end

  subject(:omnibus_nightly) { described_class.new(action: action) }

  describe '#execute' do
    before do
      allow(client)
        .to receive(:pipeline_schedules)
        .and_return(pipeline_schedules)
    end

    context 'when disabling the omnibus nightly builds' do
      it 'updates pipelines schedules and sends slack notification' do
        allow(client)
          .to receive(:edit_pipeline_schedule)

        expect(client)
          .to receive(:pipeline_schedules)
          .with(project)

        expect(client)
          .to receive(:pipeline_schedule_take_ownership)
          .with(project, 1)

        expect(client)
          .to receive(:edit_pipeline_schedule)
          .with('gitlab/omnibus-gitlab', 1, active: false)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          omnibus_nightly.execute
        end
      end
    end

    context 'when enabling the omnibus nightly builds' do
      let(:action) { :enable }

      it 'updates pipelines schedules and sends slack notification' do
        allow(client)
          .to receive(:edit_pipeline_schedule)

        expect(client)
          .to receive(:pipeline_schedules)
          .with(project)

        expect(client)
          .to receive(:pipeline_schedule_take_ownership)
          .with(project, 1)

        expect(client)
          .to receive(:edit_pipeline_schedule)
          .with('gitlab/omnibus-gitlab', 1, active: true)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          omnibus_nightly.execute
        end
      end
    end

    context 'when something goes wrong' do
      it 'sends a slack notification and raises exception' do
        allow(client)
          .to receive(:edit_pipeline_schedule)
          .and_raise(::Gitlab::Error::Error)

        expect(client)
          .to receive(:pipeline_schedules)
          .with(project)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          expect { omnibus_nightly.execute }
            .to raise_error(described_class::CouldNotUpdateError)
        end
      end
    end

    context 'with a dry-run' do
      it 'only fetches the pipeline schedules' do
        allow(client)
          .to receive(:edit_pipeline_schedule)

        expect(client)
          .to receive(:pipeline_schedules)
          .with(project)

        expect(client)
          .not_to receive(:pipeline_schedule_take_ownership)

        expect(client)
          .not_to receive(:edit_pipeline_schedule)

        allow(notifier)
          .to receive(:send_notification)

        omnibus_nightly.execute
      end
    end
  end
end
