# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Publish::CopyBlogPostToCanonical do
  subject(:service) do
    described_class.new(security_blog_merge_request)
  end

  let(:security_blog_merge_request) do
    create(:merge_request, source_branch: 'baz', assignees: [build(:user)])
  end

  let(:handbook_project) { ReleaseTools::Project::WWWGitlabCom }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }

  let(:blog_merge_request) do
    instance_spy(ReleaseTools::Security::BlogMergeRequest)
  end

  before do
    diffs = [
      build(
        :diff,
        new_path: 'sites/uncategorized/source/releases/posts/foo.html.md'
      )
    ]

    allow(client)
      .to receive(:merge_request_diffs)
      .and_return(diffs)

    allow(client)
      .to receive(:file_contents)
      .and_return('bar')

    allow(blog_merge_request)
      .to receive(:remote_issuable)
      .and_return(create(:merge_request))
  end

  describe '#execute' do
    it 'creates a merge request' do
      expect(client)
        .to receive(:create_branch)
        .with('baz-canonical', handbook_project.default_branch, handbook_project)

      expect(client)
        .to receive(:create_commit)
        .with(
          handbook_project,
          'baz-canonical',
          a_string_including(security_blog_merge_request.web_url),
          [
            a_hash_including(file_path: 'sites/uncategorized/source/releases/posts/foo.html.md')
          ]
        )

      expect(ReleaseTools::Security::BlogMergeRequest)
        .to receive(:new)
        .with(
          project: handbook_project,
          title: security_blog_merge_request.title,
          labels: security_blog_merge_request.labels,
          description: a_string_including(security_blog_merge_request.web_url),
          source_branch: 'baz-canonical',
          target_branch: handbook_project.default_branch,
          assignee_ids: security_blog_merge_request.assignees.map(&:id)
        ).and_return(blog_merge_request)

      expect(blog_merge_request)
        .to receive(:create)

      without_dry_run { service.execute }
    end

    context 'when the security blog post is in draft' do
      let(:security_blog_merge_request) do
        create(:merge_request, :draft, source_branch: 'baz', assignees: [build(:user)])
      end

      it 'creates a merge request in ready state' do
        expect(client)
          .to receive(:create_branch)

        expect(client)
          .to receive(:create_commit)

        expect(ReleaseTools::Security::BlogMergeRequest)
          .to receive(:new)
          .with(
            project: handbook_project,
            title: 'MR title',
            labels: security_blog_merge_request.labels,
            description: a_string_including(security_blog_merge_request.web_url),
            source_branch: 'baz-canonical',
            target_branch: handbook_project.default_branch,
            assignee_ids: security_blog_merge_request.assignees.map(&:id)
          ).and_return(blog_merge_request)

        expect(blog_merge_request)
          .to receive(:create)

        without_dry_run { service.execute }
      end
    end

    context 'when something goes wrong' do
      it 'raises an error' do
        allow(client)
          .to receive(:create_branch)
          .and_raise(StandardError)

        without_dry_run do
          expect { service.execute }
            .to raise_error(StandardError)
        end
      end
    end

    context 'with a dry-run' do
      it 'does nothing' do
        expect(client)
          .not_to receive(:create_branch)

        expect(client)
          .not_to receive(:create_commit)

        expect(ReleaseTools::Security::BlogMergeRequest)
          .not_to receive(:new)

        expect(blog_merge_request)
          .not_to receive(:create)

        service.execute
      end
    end
  end
end
