# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Publish::DeployBlogPost do
  let(:blog_post) do
    create(
      :merge_request,
      iid: 2,
      web_url: 'https://test.com/www-gitlab-com/-/merge_requests/1',
      sha: '123',
      assignees: [
        double('foo', username: 'foo'),
        double('bar', username: 'baz')
      ]
    )
  end

  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }

  subject(:deploy_service) { described_class.new }

  before do
    allow(client)
      .to receive(:security_blog_merge_request)
      .and_return(blog_post)

    schedule = instance_spy(ReleaseTools::ReleaseManagers::Schedule)

    allow(ReleaseTools::ReleaseManagers::Schedule)
      .to receive(:new)
      .and_return(schedule)

    release_managers = [
      double('foo', id: 1, username: 'foo'),
      double('bar', id: 2, username: 'baz')
    ]

    allow(schedule)
      .to receive(:active_release_managers)
      .and_return(release_managers)
  end

  describe '#execute' do
    it 'deploys the blog post' do
      expect(client)
        .to receive(:merge_request_approvals)

      expect(client)
        .to receive(:approve_merge_request)

      expect(client)
        .to receive(:merge_request_pipelines)

      expect(client)
        .to receive(:add_to_merge_train)

      expect(notifier)
        .to receive(:send_notification)

      without_dry_run do
        deploy_service.execute
      end
    end

    context 'when the blog post has been approved' do
      it 'skips approvals and adds the MR to the merge train' do
        allow(client)
          .to receive(:merge_request_approvals)
          .and_return(double(:approvals_by, approved_by: create(:user)))

        expect(client)
          .not_to receive(:approve_merge_request)

        expect(client)
          .to receive(:add_to_merge_train)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          deploy_service.execute
        end
      end
    end

    context 'when the pipeline is pending' do
      it 'adds the MR to the merge train' do
        allow(client)
          .to receive(:merge_request_pipelines)
          .and_return([create(:pipeline, :running)])

        expect(client)
          .to receive(:approve_merge_request)

        expect(client)
          .to receive(:merge_request_pipelines)

        expect(client)
          .to receive(:add_to_merge_train)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          deploy_service.execute
        end
      end
    end

    context 'when something goes wrong' do
      it 'sends a slack notification and raises an error' do
        allow(client)
          .to receive(:approve_merge_request)
          .and_raise(Gitlab::Error::Error)

        expect(client)
          .not_to receive(:add_to_merge_train)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          expect { deploy_service.execute }
            .to raise_error(Gitlab::Error::Error)
        end
      end
    end

    context 'with a dry-run' do
      it 'does do anything' do
        expect(client)
          .not_to receive(:approve_merge_request)

        expect(client)
          .not_to receive(:add_to_merge_train)

        expect(notifier)
          .not_to receive(:send_notification)

        deploy_service.execute
      end
    end
  end
end
