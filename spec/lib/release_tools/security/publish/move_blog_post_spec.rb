# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Publish::MoveBlogPost do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }
  let(:copier) { stub_const('ReleaseTools::Security::Publish::CopyBlogPostToCanonical', spy) }
  let(:security_blog_merge_request) { create(:merge_request) }
  let(:canonical_blog_merge_request) { create(:merge_request) }

  subject(:move_blog_post) { described_class.new }

  describe '#execute' do
    before do
      allow(move_blog_post)
        .to receive(:security_blog_merge_request)
        .and_return(security_blog_merge_request)

      allow(move_blog_post)
        .to receive(:canonical_blog_merge_request)
        .and_return(canonical_blog_merge_request)
    end

    it 'moves the blog post and closes the security MR' do
      expect(copier).to receive(:new)

      expect(client)
        .to receive(:update_merge_request)
        .with(
          'gitlab-org/security/www-gitlab-com',
          security_blog_merge_request.iid,
          { state_event: 'close' }
        )

      expect(client)
        .to receive(:create_merge_request_comment)
        .with(
          'gitlab-org/security/www-gitlab-com',
          security_blog_merge_request.iid,
          a_string_including(canonical_blog_merge_request.web_url)
        )

      expect(notifier).to receive(:new)
        .with(
          job_type: 'Move blog post',
          status: :success,
          release_type: :security
        )

      without_dry_run do
        move_blog_post.execute
      end
    end

    context 'when the security blog post merge request is not found' do
      let(:security_blog_merge_request) { nil }

      it 'sends a slack notification and raises exception' do
        expect(client)
          .not_to receive(:update_merge_request)

        expect(copier)
          .not_to receive(:new)

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Move blog post',
            status: :failed,
            release_type: :security
          )

        without_dry_run do
          expect { move_blog_post.execute }
            .to raise_error(described_class::SecurityMergeRequestNotFoundError)
        end
      end
    end

    context 'when the blog post could not be moved' do
      it 'raises an error' do
        allow(copier)
          .to receive(:new)
          .and_raise

        expect(notifier).to receive(:new)
          .with(
            job_type: 'Move blog post',
            status: :failed,
            release_type: :security
          )

        without_dry_run do
          expect { move_blog_post.execute }
            .to raise_error(StandardError)
        end
      end
    end

    context 'when something else goes wrong' do
      it 'sends a slack notification and raises exception' do
        allow(client)
          .to receive(:update_merge_request)
          .and_raise

        expect(notifier).to receive(:new)
          .with(
            job_type: 'Move blog post',
            status: :failed,
            release_type: :security
          )

        without_dry_run do
          expect { move_blog_post.execute }
            .to raise_error(StandardError)
        end
      end
    end

    context 'with a dry-run' do
      it 'does nothing' do
        expect(copier)
          .not_to receive(:new)

        expect(notifier)
          .not_to receive(:new)

        move_blog_post.execute
      end
    end
  end
end
