# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::ReleasePreparation::GenerateBlogPost do
  subject(:execute) { described_class.new.execute }

  let(:blog_post) { instance_spy(ReleaseTools::PatchRelease::BlogMergeRequest) }
  let(:coordinator) { instance_spy(ReleaseTools::PatchRelease::Coordinator) }
  let(:issue_crawler) { instance_spy(ReleaseTools::Security::IssueCrawler) }
  let(:blog_post_mr) { build(:merge_request) }
  let(:notifier) { instance_spy(ReleaseTools::Slack::ReleaseJobEndNotifier) }

  let(:versions) do
    [
      ReleaseTools::Version.new('16.1.1'),
      ReleaseTools::Version.new('16.0.1'),
      ReleaseTools::Version.new('15.11.1')
    ]
  end

  before do
    allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(versions)

    allow(ReleaseTools::PatchRelease::Coordinator)
      .to receive(:new)
      .and_return(coordinator)

    allow(coordinator)
      .to receive(:merge_requests)
      .and_return([])

    allow(ReleaseTools::Security::IssueCrawler)
      .to receive(:new)
      .and_return(issue_crawler)

    allow(issue_crawler)
      .to receive(:related_security_issues)
      .and_return([])

    allow(ReleaseTools::PatchRelease::BlogMergeRequest)
      .to receive(:new)
      .and_return(blog_post)

    allow(blog_post)
      .to receive(:create)
      .and_return(nil)

    allow(blog_post)
      .to receive(:url)
      .and_return(blog_post_mr.web_url)

    allow(ReleaseTools::GitlabClient)
      .to receive(:next_security_tracking_issue)
      .and_return(create(:issue))

    allow(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .and_return(notifier)
  end

  it 'creates blog post' do
    expect(coordinator)
      .to receive(:merge_requests)
      .with(with_patch_version: true)

    expect(blog_post).to receive(:create)

    expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .with(
        job_type: 'Generate blog post',
        status: :success,
        release_type: :security,
        extra_string: "Blog MR: #{blog_post_mr.web_url}"
      )

    expect(ReleaseTools::GitlabClient)
      .to receive(:create_merge_request_comment)

    expect(notifier).to receive(:send_notification)

    without_dry_run { execute }
  end

  context 'with dry run' do
    it 'does not create MR' do
      expect(coordinator)
        .to receive(:merge_requests)
        .with(with_patch_version: true)

      expect(blog_post).not_to receive(:create)
      expect(blog_post).to receive(:generate_blog_content)

      expect(ReleaseTools::Slack::ReleaseJobEndNotifier).not_to receive(:new)

      execute
    end
  end

  context 'when something goes wrong' do
    before do
      allow(blog_post)
        .to receive(:create)
        .and_raise(::Gitlab::Error::Error)
    end

    it 'sends a slack notification and raises exception' do
      expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
        .to receive(:new)
        .with(
          job_type: 'Generate blog post',
          status: :failed,
          release_type: :security
        )

      without_dry_run do
        expect { execute }.to raise_error(::Gitlab::Error::Error)
      end
    end
  end
end
