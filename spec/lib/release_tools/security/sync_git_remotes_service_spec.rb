# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::SyncGitRemotesService do
  let(:git_versions) { ['2.38.3', '2.39.2', '2.37.5'] }
  let(:service) { described_class.new(git_versions) }

  describe '#execute' do
    it 'syncs tags for the versions' do
      expect(service).to receive(:sync_tags)
        .with(ReleaseTools::Project::Git, '2.38.3', '2.39.2', '2.37.5')

      service.execute
    end
  end
end
