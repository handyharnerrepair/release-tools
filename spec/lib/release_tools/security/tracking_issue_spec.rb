# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::TrackingIssue do
  before do
    allow(ReleaseTools::Versions)
      .to receive(:next_versions)
      .and_return(['16.6.4', '16.5.7', '16.4.10'])

    allow(ReleaseTools::ProductMilestone).to receive(:current)
      .and_return(build(:gitlab_response, title: '16.7'))
  end

  subject { described_class.new }

  describe '#title' do
    subject { described_class.new.title }

    it { is_expected.to eq('Security release: 16.7.x, 16.6.x, 16.5.x') }

    context 'when performing a critical security release' do
      subject do
        ClimateControl.modify(SECURITY: 'critical') do
          described_class.new.title
        end
      end

      it { is_expected.to eq('Critical security release: 16.7.x, 16.6.x, 16.5.x') }
    end
  end

  describe '#confidential?' do
    it { is_expected.to be_confidential }
  end

  describe '#labels' do
    subject { described_class.new.labels }

    it { is_expected.to include('upcoming security release') }
    it { is_expected.to include('security') }
    it { is_expected.to include('meta') }
  end

  describe '#project' do
    subject { described_class.new.project }

    it { is_expected.to eq(ReleaseTools::Project::GitlabEe) }
  end

  describe '#versions_title' do
    it 'includes a future version and the last two released versions' do
      expect(subject.versions_title).to eq('16.7.x, 16.6.x, 16.5.x')
    end

    context 'with multiple security releases enabled' do
      it 'includes the upcoming versions for the release cadence' do
        enable_feature(:multiple_security_releases)

        allow(ReleaseTools::GitlabReleasesClient)
          .to receive(:current_minor_for_date)
          .and_return('16.6')

        allow(ReleaseTools::GitlabReleasesClient)
          .to receive(:previous_minors)
          .and_return(['16.6', '16.5', '16.4'])

        expect(subject.versions_title).to eq('16.6.x, 16.5.x, 16.4.x')
      end
    end
  end

  describe '#version' do
    it 'returns the next version' do
      expect(ReleaseTools::ProductMilestone).to receive(:current)

      expect(subject.version).to eq('16.7.0')
    end
  end

  describe '#description' do
    it 'returns the tracking issue template' do
      expect(subject.description).not_to be_nil
    end
  end

  describe '#due_date' do
    it 'returns nil' do
      expect(subject.due_date).to be_nil
    end
  end

  describe '#assignees' do
    let(:schedule) { instance_spy(ReleaseTools::ReleaseManagers::Schedule) }

    before do
      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)
    end

    it 'returns assignees ids of the active release managers' do
      expect(schedule)
        .to receive(:active_release_managers)
        .and_return([double('user1', id: 1), double('user2', id: 2)])

      expect(subject.assignees).to eq([1, 2])
    end

    context 'with multiple security releases enabled' do
      it 'returns assignees ids of the scheduled release managers' do
        enable_feature(:multiple_security_releases)

        allow(ReleaseTools::GitlabReleasesClient)
          .to receive(:version_for_date)
          .and_return('16.1')

        expect(schedule)
          .to receive(:authorized_release_managers)
          .with('16.1.0')
          .and_return([double('user3', id: 3), double('user4', id: 4)])

        expect(subject.assignees).to eq([3, 4])
      end
    end
  end
end
