# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::TagNotification do
  include SlackWebhookHelpers

  let(:webhook_url) { 'https://slack.example.com/' }

  describe '.webhook_url' do
    it 'returns blank when not set' do
      ClimateControl.modify(SLACK_TAG_URL: nil) do
        expect(described_class.webhook_url).to eq('')
      end
    end

    it 'returns ENV value when set' do
      ClimateControl.modify(SLACK_TAG_URL: webhook_url) do
        expect(described_class.webhook_url).to eq(webhook_url)
      end
    end
  end

  describe '.release' do
    let(:project) { ReleaseTools::Project::Gitaly }
    let(:version) { ReleaseTools::Version.new('10.4.20') }
    let(:message) { "_Liz Lemon_ tagged `#{version}` on `#{project}`" }

    before do
      allow(ReleaseTools::SharedStatus).to receive(:user).and_return('Liz Lemon')
    end

    around do |ex|
      ClimateControl.modify(SLACK_TAG_URL: webhook_url) do
        ex.run
      end
    end

    context 'with a non-security release' do
      it 'posts an attachment' do
        expect_post(
          json: {
            attachments: [{
              fallback: '',
              color: 'good',
              text: "<https://gitlab.com/gitlab-org/gitaly/-/tags/#{version}|#{message}>",
              mrkdwn_in: ['text']
            }]
          }
        ).and_return(response(200))

        described_class.release(project, version)
      end
    end

    context 'with a security release' do
      around do |ex|
        ClimateControl.modify(SECURITY: 'true') do
          ex.run
        end
      end

      it 'posts an attachment' do
        expect_post(
          json: {
            attachments: [{
              fallback: '',
              color: 'good',
              text: "<https://gitlab.com/gitlab-org/security/gitaly/-/tags/#{version}|#{message} as a security release>",
              mrkdwn_in: ['text']
            }]
          }
        ).and_return(response(200))

        described_class.release(project, version)
      end
    end
  end
end
