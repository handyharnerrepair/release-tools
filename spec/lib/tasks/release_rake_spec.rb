# frozen_string_literal: true

require 'rake_helper'

describe 'release tasks', :rake do
  describe 'issue', task: 'release:issue' do
    it_behaves_like 'handling invalid version args'

    it 'creates a release issue for the specified version' do
      expect(ReleaseTools::Tasks::Release::Issue).to receive(:new)
        .with('1.0.1')
        .and_return(instance_double(ReleaseTools::Tasks::Release::Issue, execute: true))

      task.invoke('1.0.1')
    end
  end

  describe 'patch_blog_post', task: 'release:patch_blog_post' do
    let(:security_content) { nil }
    let(:patch_content) { %w[a b] }
    let(:coordinator) { instance_double(ReleaseTools::PatchRelease::Coordinator, merge_requests: patch_content) }

    let(:blog_post) do
      ReleaseTools::PatchRelease::BlogMergeRequest.new(
        project: ReleaseTools::Project::WWWGitlabCom,
        patch_content: patch_content,
        security_content: security_content
      )
    end

    before do
      allow(ReleaseTools::PatchRelease::Coordinator).to receive(:new).and_return(coordinator)

      allow(blog_post).to receive(:create).and_return(nil)
      allow(blog_post).to receive(:generate_blog_content).and_return(nil)
      allow(blog_post).to receive(:exists?).and_return(false)
      allow(blog_post).to receive(:title).and_return('Title')
      allow(blog_post).to receive(:description).and_return('description')
      allow(blog_post).to receive(:remote_issuable).and_return(build(:issue))
    end

    context 'in dry run mode' do
      it 'does not create MR' do
        expect(ReleaseTools::PatchRelease::BlogMergeRequest).to receive(:new).with(
          project: ReleaseTools::Project::WWWGitlabCom,
          patch_content: patch_content,
          security_content: nil
        ).and_return(blog_post)

        expect(blog_post).not_to receive(:create)

        task.invoke('1.0.1')
      end
    end

    context 'with a version' do
      it 'creates a blog post' do
        expect(ReleaseTools::PatchRelease::BlogMergeRequest).to receive(:new).with(
          project: ReleaseTools::Project::WWWGitlabCom,
          patch_content: patch_content,
          security_content: nil
        ).and_return(blog_post)

        expect(blog_post).to receive(:create)

        without_dry_run { task.invoke('1.0.1') }
      end
    end

    context 'without a version' do
      let(:security_content) { %w[x y] }
      let(:crawler) { instance_double(ReleaseTools::Security::IssueCrawler, related_security_issues: security_content) }

      let(:generate_blog_post) do
        instance_double(ReleaseTools::Security::ReleasePreparation::GenerateBlogPost)
      end

      it 'calls Security::ReleasePreparation::GenerateBlogPost' do
        expect(ReleaseTools::Security::ReleasePreparation::GenerateBlogPost)
          .to receive(:new)
          .and_return(generate_blog_post)

        expect(generate_blog_post).to receive(:execute)

        expect(ReleaseTools::PatchRelease::BlogMergeRequest).not_to receive(:new)
        expect(ReleaseTools::Security::IssueCrawler).not_to receive(:new)

        without_dry_run { task.invoke }
      end
    end
  end
end
